/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ConectorBD;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadPoolExecutor.CallerRunsPolicy;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.swing.JOptionPane;

import Utils.StringMatcher;
import Gui.InsertarEmpGui;
import datechooser.model.DaysGrid;
import oracle.jdbc.driver.OracleCallableStatement;
import oracle.jdbc.driver.OracleResultSet;
import oracle.jdbc.driver.OracleTypes;
import oracle.jdbc.pool.OracleDataSource;

/**
 * @author Andres Mauricio
 * Actualizado Oscar Leif
 */
public class ConexionBDOracle {
    public final static String CLASE_CONEXION = "oracle.jdbc.driver.OracleDriver";

    private String ipAdress;
    private String user;
    private String password;
    private String url;
    private Connection conn;
    private Statement stmt;
    private int jobId;
    /**
     * This is a default constructor, 
     */
    public ConexionBDOracle(){
        url = "jdbc:oracle:thin:@192.168.1.104:1521:xe";
        user = "oscar";
        password = "oscar";//The default or Old password was ORACLE
        conectar();
    }
    public ConexionBDOracle(String ip)
    {
    	url = "jdbc:oracle:thin:@"+ip+":1521:xe";
        user = "oscar";
        password = "oscar";//The default or Old password was ORACLE
        conectar();
    }

    public ConexionBDOracle(String ipadd, String us, String pass, String u){
        ipAdress = ipadd;
        user = us;
        password = pass;
        url = u;
        conectar();
    }
    public ConexionBDOracle(String ipadd, String us, String pass){
        ipAdress = ipadd;
        user = us;
        password = pass;
        url = "jdbc:oracle:thin:@"+ipadd+":1521:xe";
        conectar();
    }
    public ConexionBDOracle(String ipadd, String us, String pass, String instancia, String puerto)
    {
    	ipAdress = ipadd;
        user = us;
        password = pass;
        url = String.format("jdbc:oracle:thin:@%s:%s:%s", ipadd,puerto,instancia);
        System.out.println(url);
        conectar();
    }
        ///Comment
    private void conectar(){
        try{
            Class.forName( CLASE_CONEXION ).newInstance();
            DriverManager.setLoginTimeout(0x2);
            
            //conn.setNetworkTimeout(null, 5000);
            conn = DriverManager.getConnection(url, user, password);
            //Statement s = conn.createStatement();
            //s.execute("alter session set NLS_DATE_FORMAT='YYYY-MM-dd'");
            stmt = conn.createStatement();
            stmt.execute("alter session set NLS_DATE_FORMAT='YYYY-MM-dd'");
            stmt.close();
            conn.setAutoCommit(false);
            //JOptionPane.showMessageDialog(null,"Se conecto a la base de datos Oracle " , "Mensajes",JOptionPane.INFORMATION_MESSAGE);
        }
        catch(SQLException ex){
            //JOptionPane.showMessageDialog(null,"Se genero el siguiente error " + ex, "Mensajes",JOptionPane.ERROR_MESSAGE);
            System.out.println(ex);
            System.err.println("No hay Conexion de Oracle");
        }
        catch(ClassNotFoundException ex){
           // JOptionPane.showMessageDialog(null,"Se genero el siguiente error " + ex, "Mensajes",JOptionPane.ERROR_MESSAGE);
            System.out.println("Clase no encontrada: " + ex);
        }
        catch(Exception ex){
            //JOptionPane.showMessageDialog(null,"Se genero el siguiente error " + ex, "Mensajes",JOptionPane.ERROR_MESSAGE);
            System.out.println("Otra excepcion: " + ex);
        }
    }

     public boolean executeUpdateStatement(String cad){
        int r = 0;
        try{
            stmt = conn.createStatement();
            r = stmt.executeUpdate(cad);
            //System.out.println("Actualizacion realizada... "+r);
            //conn.commit();
            stmt.close();
            //stmt = null;
            return true;
        }
        catch(Exception ex){
            System.out.println("\n"+"No se pudo efecutar la grabacion... "+ex);
            System.err.println("\n "+ cad);
            return false;
        }
    }

     public ResultSet executeQueryStatement(String cad){
        ResultSet res = null;
        try{
            stmt = conn.createStatement();
            res= stmt.executeQuery(cad);
            System.out.println("Consulta realizada...");
        }
        catch(Exception ex){
                System.out.println("No se pudo efectuar la consulta... "+ex);
        }
        return res;
    }

    public void closeConnection(){
        try{
            conn.close();
        }
        catch(SQLException e){
        }
    }
    
    /**
     * 
     * @return el objeto conexion de oracle
     */
    public Connection getConnection()
    {
        return conn;
    }
    /**
     * testConnection usando un simple query en java se puede conocer el estado de
     *la connecion de oracle. Retorna true si esta se puede connectar a oracle, false
     *si esta no se pudo connectar.
     *
     */ 
    public boolean testConecction()
    {
        boolean conectado = false;
        String validationQuery = "Select 1 from dual";
        ResultSet result=null;
        try {
            stmt = conn.createStatement();
            result = stmt.executeQuery(validationQuery);
            conectado = true;
        } catch (SQLException ex) {
            Logger.getLogger(ConexionBDOracle.class.getName()).log(Level.SEVERE, null, ex);
            return conectado = false;
        }
        catch(Throwable e)
        {
            System.err.println("No hay conexiones con Oracle");
        }
        return conectado;
        
    }
    public java.util.List<String> ListDarDep(String procedure)
    {
    	java.util.List<String> listDepartmentes = new ArrayList<String>();
    	try {			
			OracleCallableStatement oraCallStmt = null;
			OracleResultSet deptResultSet = null;
			
			oraCallStmt = (OracleCallableStatement) conn.prepareCall("{? = call demo.DEPTOSYEMPDISPO.GETDEPTOS()}"
	            );			
			oraCallStmt.registerOutParameter(1, OracleTypes.CURSOR);
			oraCallStmt.execute();
			deptResultSet = (OracleResultSet) oraCallStmt.getCursor(1);
			
			while(deptResultSet.next())
			{
				System.out.println(deptResultSet.getString("NAME"));
				listDepartmentes.add(deptResultSet.getString("NAME"));
			}
			oraCallStmt.close();
			deptResultSet.close();
		} catch (SQLException e) {
			//e.printStackTrace();
			System.out.println(e.getMessage());
		}
		return listDepartmentes;
    }
	public List<String> darListaEmpleadosId() {
		List<String> listaEmpleados = new ArrayList<String>();
    	try {			
    		OracleCallableStatement oraCallStmt = null;
			OracleResultSet deptResultSet = null;
			
			oraCallStmt = (OracleCallableStatement) conn.prepareCall("{? = call demo.DEPTOSYEMPDISPO.GETALLEMPLOYEES()}"
	            );			
			oraCallStmt.registerOutParameter(1, OracleTypes.CURSOR);
			oraCallStmt.execute();
			deptResultSet = (OracleResultSet) oraCallStmt.getCursor(1);
			
			while(deptResultSet.next())
			{
				//System.out.println(deptResultSet.getString("EMPLOYEE_ID"));
				listaEmpleados.add(Integer.toString(deptResultSet.getInt("EMPLOYEE_ID")));
			}
			oraCallStmt.close();
			deptResultSet.close();
			
		} catch (SQLException e) {
			//e.printStackTrace();
			System.out.println(e.getMessage());
		}
		return listaEmpleados;
	}
	
	public List<String> darListaEstadoEmpleadoDesc()
	{
		List<String> listaEstados = new ArrayList<String>();
		try {			
    		OracleCallableStatement oraCallStmt = null;
			OracleResultSet deptResultSet = null;
			
			oraCallStmt = (OracleCallableStatement) conn.prepareCall("{? = call demo.DEPTOSYEMPDISPO.GETSTATUSEMPLOYEE()}"
	            );			
			oraCallStmt.registerOutParameter(1, OracleTypes.CURSOR);
			oraCallStmt.execute();
			deptResultSet = (OracleResultSet) oraCallStmt.getCursor(1);
			
			while(deptResultSet.next())
			{
				listaEstados.add(deptResultSet.getString("DESCRIPTION"));
			}
			oraCallStmt.close();
			deptResultSet.close();
		} catch (SQLException e) {
			//e.printStackTrace();
			System.out.println(e.getMessage());
		}
		return listaEstados;
	}
	
    
    public java.util.List<String> listAvaJobs(String procedure)
    {
    	java.util.List<String> listJobs = new ArrayList<String>();
    	try {			
			OracleCallableStatement oraCallStmt = null;
			OracleResultSet deptResultSet = null;
			
			oraCallStmt = (OracleCallableStatement) conn.prepareCall("{? = call demo.DEPTOSYEMPDISPO.GETEMP()}"
	            );			
			oraCallStmt.registerOutParameter(1, OracleTypes.CURSOR);
			oraCallStmt.execute();
			deptResultSet = (OracleResultSet) oraCallStmt.getCursor(1);//This is the cursor with the data.
			
			while(deptResultSet.next())
			{
				System.out.println(deptResultSet.getString("FUNCTION"));
				listJobs.add(deptResultSet.getString("FUNCTION"));
			}
			oraCallStmt.close();
			deptResultSet.close();
		} catch (SQLException e) {
			//e.printStackTrace();
			System.out.println(e.getMessage());
		}
		return listJobs;
    }
    
   
    public List<String> darListaEmpleadosNombre(String nameSearch)
    {
    	List<String> listaEmp = new ArrayList<String>();
    	try {			
			OracleCallableStatement oraCallStmt = null;
			OracleResultSet deptResultSet = null;
			
			oraCallStmt = (OracleCallableStatement) conn.prepareCall("{? = call demo.DEPTOSYEMPDISPO.GETEMPLOYEE(?)}"
	            );
			
			oraCallStmt.registerOutParameter(1, OracleTypes.CURSOR);
			oraCallStmt.setString(2, nameSearch);
			oraCallStmt.execute();
			deptResultSet = (OracleResultSet) oraCallStmt.getCursor(1);//This is the cursor with the data.
			String sp = " ";
			while(deptResultSet.next())
			{
				String Resultado = deptResultSet.getInt("EMPLOYEE_ID") +sp+deptResultSet.getString("LAST_NAME")+sp+deptResultSet.getString("FIRST_NAME") ;
				listaEmp.add(Resultado);
			}
			oraCallStmt.close();
			deptResultSet.close();
		} catch (SQLException e) {
			//e.printStackTrace();
			System.out.println(e.getMessage());
		}
    	
		return listaEmp;
    }
    public int returnJobId(String nameJob)
    {
    	int returnID=0;
    	OracleCallableStatement oraCallStmt = null;
		OracleResultSet deptResultSet = null;
		
		try {
			oraCallStmt = (OracleCallableStatement) conn.prepareCall("{? = call demo.DEPTOSYEMPDISPO.GETJOBBYNAME(?)}");
			oraCallStmt.registerOutParameter(1, OracleTypes.CURSOR);
			oraCallStmt.setString(2, nameJob);
			oraCallStmt.execute();
			
			deptResultSet = (OracleResultSet) oraCallStmt.getCursor(1);//This is the cursor with the data.
			
			while(deptResultSet.next())
			{
				//System.out.println(deptResultSet.getString("FIRST_NAME"));
				//listaEmp.add(deptResultSet.getString("FIRST_NAME"));
				returnID = deptResultSet.getInt("JOB_ID");
				
			}
			oraCallStmt.close();
			deptResultSet.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return returnID;

    }
    
    public int returnDepartmentId(String nameDepartment)
    {
    	int value=0;
    	OracleCallableStatement oraCallStmt = null;
		OracleResultSet deptResultSet = null;
		try {
			oraCallStmt = (OracleCallableStatement) conn.prepareCall("{? = call demo.DEPTOSYEMPDISPO.GETDEPNAME(?)}");
			oraCallStmt.registerOutParameter(1, OracleTypes.CURSOR);
			oraCallStmt.setString(2, nameDepartment);
			oraCallStmt.execute();
			
			deptResultSet = (OracleResultSet) oraCallStmt.getCursor(1);//This is the cursor with the data.
			
			while(deptResultSet.next())
			{
				//System.out.println(deptResultSet.getString("FIRST_NAME"));
				//listaEmp.add(deptResultSet.getString("FIRST_NAME"));
				value = deptResultSet.getInt("DEPARTMENT_ID");
				
			}
			oraCallStmt.close();
			deptResultSet.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return value;
    	
    }
    
    public List<String> darListaEmpleadosId(int ID)
    {
    	List<String> listaEmp = new ArrayList<String>();
    	try {			
			OracleCallableStatement oraCallStmt = null;
			OracleResultSet deptResultSet = null;
			
			oraCallStmt = (OracleCallableStatement) conn.prepareCall("{? = call demo.DEPTOSYEMPDISPO.GETEMPLOYEE_ID(?)}"
	            );
			
			oraCallStmt.registerOutParameter(1, OracleTypes.CURSOR);
			oraCallStmt.setInt(2, ID);
			oraCallStmt.execute();
			deptResultSet = (OracleResultSet) oraCallStmt.getCursor(1);//This is the cursor with the data.
			String sp = " ";
			while(deptResultSet.next())
			{
				
				System.out.println(deptResultSet.getString("LAST_NAME"));
				String Resultado = deptResultSet.getInt("EMPLOYEE_ID") +sp+deptResultSet.getString("LAST_NAME")+sp+deptResultSet.getString("FIRST_NAME") ;
				listaEmp.add(Resultado);
			}
			oraCallStmt.close();
			deptResultSet.close();
		} catch (SQLException e) {
			//e.printStackTrace();
			System.out.println(e.getMessage());
		}
    	return listaEmp;
    		
    }
    public List<String> darListaComisionEmpleadosId(int ID)
    {
    	List<String> listaEmp = new ArrayList<String>();
    	try {			
			OracleCallableStatement oraCallStmt = null;
			OracleResultSet deptResultSet = null;
			
			oraCallStmt = (OracleCallableStatement) conn.prepareCall("{? = call demo.DEPTOSYEMPDISPO.GETEMPLOYEE_ID(?)}"
	            );
			
			oraCallStmt.registerOutParameter(1, OracleTypes.CURSOR);
			oraCallStmt.setInt(2, ID);
			oraCallStmt.execute();
			deptResultSet = (OracleResultSet) oraCallStmt.getCursor(1);//This is the cursor with the data.
			String sp = " ";
			while(deptResultSet.next())
			{
				
				//System.out.println(deptResultSet.getString("LAST_NAME"));
				int Resultado = deptResultSet.getInt("COMMISSION");
				listaEmp.add(Integer.toString(Resultado));
			}
			oraCallStmt.close();
			deptResultSet.close();
		} catch (SQLException e) {
			//e.printStackTrace();
			System.out.println(e.getMessage());
		}
    	return listaEmp;
    		
    }
    public List<String> darSalarioEmpleadoId(String Id)
    {
    	StringMatcher s = new StringMatcher();
    	List<String> empleadoSalario = new ArrayList<String>();
    	
    	boolean isId= s.isNumber(Id);
    	if(isId && Id != null)
    	{
    		int id = Integer.parseInt(Id);
    		try {
    			OracleResultSet deptResultSet = null;
    			OracleCallableStatement oraCallStmt = null;
    			
				oraCallStmt = (OracleCallableStatement) conn.prepareCall("{? = call demo.DEPTOSYEMPDISPO.GET_EMPLOYEE_SALARY_BYID(?)}");
				oraCallStmt.registerOutParameter(1, OracleTypes.CURSOR);
				oraCallStmt.setInt(2, id);
				oraCallStmt.execute();
				deptResultSet = (OracleResultSet) oraCallStmt.getCursor(1);//This is the cursor with the data.
				while(deptResultSet.next())
				{
					//System.out.println(deptResultSet.getString("LAST_NAME"));
					int salario = deptResultSet.getInt("SALARY");
					String Resultado = Integer.toString(salario);
					empleadoSalario.add(Resultado);
				}
				oraCallStmt.close();
				deptResultSet.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	return empleadoSalario;
    }
    
    public List<String> darEstadoEmpleadoIdByEmpID(int ID)
    {
    	List<String> listaEmp = new ArrayList<String>();
    	try {			
			OracleCallableStatement oraCallStmt = null;
			OracleResultSet deptResultSet = null;
			
			oraCallStmt = (OracleCallableStatement) conn.prepareCall("{? = call demo.DEPTOSYEMPDISPO.GETEMPLOYEE_ID(?)}"
	            );
			
			oraCallStmt.registerOutParameter(1, OracleTypes.CURSOR);
			oraCallStmt.setInt(2, ID);
			oraCallStmt.execute();
			deptResultSet = (OracleResultSet) oraCallStmt.getCursor(1);//This is the cursor with the data.
			String sp = " ";
			while(deptResultSet.next())
			{
				
				//System.out.println(deptResultSet.getString("LAST_NAME"));
				String Resultado = deptResultSet.getString("STATUS_ID") ;
				listaEmp.add(Resultado);
			}
			oraCallStmt.close();
			deptResultSet.close();
		} catch (SQLException e) {
			//e.printStackTrace();
			System.out.println(e.getMessage());
		}
    	return listaEmp;
    }
    public List<String> darEstadoEmpleadoIDByEmpID(int ID)
    {
    	List<String> listaEmp = new ArrayList<String>();
    	try {			
			OracleCallableStatement oraCallStmt = null;
			OracleResultSet deptResultSet = null;
			
			oraCallStmt = (OracleCallableStatement) conn.prepareCall("{? = call demo.DEPTOSYEMPDISPO.GETEMPLOYEE_ID(?)}"
	            );
			
			oraCallStmt.registerOutParameter(1, OracleTypes.CURSOR);
			oraCallStmt.setInt(2, ID);
			oraCallStmt.execute();
			deptResultSet = (OracleResultSet) oraCallStmt.getCursor(1);//This is the cursor with the data.
			String sp = " ";
			while(deptResultSet.next())
			{
				
				//System.out.println(deptResultSet.getString("LAST_NAME"));
				String Resultado = deptResultSet.getString("STATUS_ID") ;
				if(Resultado=="")
					listaEmp.add("0");
				listaEmp.add(Resultado);
			}
			oraCallStmt.close();
			deptResultSet.close();
		} catch (SQLException e) {
			//e.printStackTrace();
			System.out.println(e.getMessage());
		}
    	return listaEmp;
    }
    public List<String> darIdEstadoEmpleadoByDesc(String description)
    {
    	//GETSTATUS_ID_BY_DESC
    	List<String> listaEmp = new ArrayList<String>();
    	try {			
			OracleCallableStatement oraCallStmt = null;
			OracleResultSet deptResultSet = null;
			
			oraCallStmt = (OracleCallableStatement) conn.prepareCall("{? = call demo.DEPTOSYEMPDISPO.GETSTATUS_ID_BY_DESC(?)}"
	            );
			
			oraCallStmt.registerOutParameter(1, OracleTypes.CURSOR);
			oraCallStmt.setString(2, description);
			oraCallStmt.execute();
			deptResultSet = (OracleResultSet) oraCallStmt.getCursor(1);//This is the cursor with the data.
			while(deptResultSet.next())
			{
				
				//System.out.println(deptResultSet.getString("LAST_NAME"));
				String Resultado = deptResultSet.getString("STATUS_ID") ;
				if(Resultado=="")
					listaEmp.add("0");
				listaEmp.add(Resultado);
			}
			oraCallStmt.close();
			deptResultSet.close();
		} catch (SQLException e) {
			//e.printStackTrace();
			System.out.println(e.getMessage());
		}
    	return listaEmp;
    	
    }
    public List<String> darEstadoEmpleadoDesc(String ID)
    {
    	List<String> listaEmp = new ArrayList<String>();
    	StringMatcher s =new StringMatcher();
    	if(ID == null)
    	{
    		ID = "null";
    	}
    	boolean isNumber = s.isNumber(ID);
    	if(isNumber)
    	{
    	try {			
			OracleCallableStatement oraCallStmt = null;
			OracleResultSet deptResultSet = null;
			
			oraCallStmt = (OracleCallableStatement) conn.prepareCall("{? = call demo.DEPTOSYEMPDISPO.GETSTATUS_DESC_BY_ID(?)}");
			
			oraCallStmt.registerOutParameter(1, OracleTypes.CURSOR);
			oraCallStmt.setInt(2, Integer.parseInt(ID));
			oraCallStmt.execute();
			deptResultSet = (OracleResultSet) oraCallStmt.getCursor(1);//This is the cursor with the data.
			String sp = " ";
			while(deptResultSet.next())
			{
				
				//System.out.println(deptResultSet.getString("LAST_NAME"));
				String Resultado = deptResultSet.getString("DESCRIPTION") ;
				if(Resultado=="")
					listaEmp.add("0");
				listaEmp.add(Resultado);
			}
			oraCallStmt.close();
			deptResultSet.close();
		} catch (SQLException e) {
			//e.printStackTrace();
			System.out.println(e.getMessage());
		}
    	}
    	return listaEmp;
    }
    public List<String> darListaEmpleadosApellido(String lastName)
    {
    	List<String> listaEmp = new ArrayList<String>();
    	try {			
			OracleCallableStatement oraCallStmt = null;
			OracleResultSet deptResultSet = null;
			
			oraCallStmt = (OracleCallableStatement) conn.prepareCall("{? = call demo.DEPTOSYEMPDISPO.GETEMPLOYEE_LASTNAME(?)}"
	            );
			
			oraCallStmt.registerOutParameter(1, OracleTypes.CURSOR);
			oraCallStmt.setString(2, lastName);
			oraCallStmt.execute();
			deptResultSet = (OracleResultSet) oraCallStmt.getCursor(1);//This is the cursor with the data.
			String sp = " ";
			while(deptResultSet.next())
			{
				System.out.println(deptResultSet.getString("LAST_NAME"));
				String Resultado = deptResultSet.getInt("EMPLOYEE_ID") +sp+deptResultSet.getString("LAST_NAME")+sp+deptResultSet.getString("FIRST_NAME") ;
				listaEmp.add(Resultado);
			}
			oraCallStmt.close();
			deptResultSet.close();
		} catch (SQLException e) {
			//e.printStackTrace();
			System.out.println(e.getMessage());
		}
		return listaEmp;
    	
    }
    public List<String> darListaEmpleadosPorSueldo(int valorA, int valorB)
    {
    	List<String> listaEmp = new ArrayList<String>();
    	try {			
			OracleCallableStatement oraCallStmt = null;
			OracleResultSet deptResultSet = null;
			
			oraCallStmt = (OracleCallableStatement) conn.prepareCall("{? = call demo.DEPTOSYEMPDISPO.GETEMPLOYEE_BYSALARY(?,?) }"
	            );
			
			oraCallStmt.registerOutParameter(1, OracleTypes.CURSOR);
			oraCallStmt.setInt(2, valorA);
			oraCallStmt.setInt(3, valorB);
			oraCallStmt.execute();
			deptResultSet = (OracleResultSet) oraCallStmt.getCursor(1);//This is the cursor with the data.
			String sp = " ";
			while(deptResultSet.next())
			{
				System.out.println(deptResultSet.getString("LAST_NAME"));
				String Resultado = deptResultSet.getInt("EMPLOYEE_ID") +sp+deptResultSet.getString("LAST_NAME")+sp+deptResultSet.getString("FIRST_NAME")+sp+"Sueldo: "+deptResultSet.getInt("SALARY");
				listaEmp.add(Resultado);
			}
			oraCallStmt.close();
			deptResultSet.close();			
		} catch (SQLException e) {
			//e.printStackTrace();
			System.out.println(e.getMessage());
		}
		return listaEmp;
    	
    }
    
    public boolean  registrarEmpleado(int id, String name, String lastName, String MiddleInitial
    		,int jobId, int managerId,Date date ,int salary, int commission, int departmentId)
    {
    	boolean registro = false;
    	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
    	Date sqlDate;
		try {
			sqlDate = new Date(simpleDateFormat.parse("12/02/2012").getTime());

			CallableStatement storeProc = conn.prepareCall("{call demo.DEPTOSYEMPDISPO.ADDEMPLOYEE(?,?,?,?,?,?,?,?,?,?)}");
			storeProc.setInt(1, id);
			storeProc.setString(2, lastName);
			storeProc.setString(3, name);
			storeProc.setString(4, MiddleInitial);
			storeProc.setInt(5, jobId);
			storeProc.setInt(6, managerId);
			storeProc.setDate(7, date);
			storeProc.setInt(8, salary);
			storeProc.setInt(9, commission);
			storeProc.setInt(10, departmentId);
			
			storeProc.execute();
			registro=true;
			storeProc.close();
			conn.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			//JOptionPane.showMessageDialog(InsertarEmpGui.this, e.getMessage());

		}
		catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	return registro;
    	
    }
    
    public boolean actualizarSueldoPorIdEmp(int id, int sueldoNuevo)
    {
    	boolean update = false;
    	//INCSALARIO
    	try {
			CallableStatement storeProc = conn.prepareCall("{call demo.DEPTOSYEMPDISPO.INCSALARIO(?,?,?)}");
			storeProc.setInt(1, id);
			storeProc.setInt(2, sueldoNuevo);
			storeProc.setString(3,"What is this ?");
			storeProc.execute();
			update=true;
			System.out.println("Here");
			conn.commit();
			storeProc.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
    	return update;
    }
    public boolean actualizarEstadoEmpleado(int id,int status_id)
    {
    	boolean update =false;
    	//UPDATE_STATUS_EMPLOYEE(ID IN NUMBER, IN_STATUS_ID IN NUMBER)
    	try {
			CallableStatement storeProc = conn.prepareCall("{call demo.DEPTOSYEMPDISPO.UPDATE_STATUS_EMPLOYEE(?,?)}");
			storeProc.setInt(1, id);
			storeProc.setInt(2, status_id);
			//storeProc.setString(3,"What is this ?");
			storeProc.execute();
			update=true;
			System.out.println("Here");
			conn.commit();
			storeProc.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
    	return update;
    }
	public List<String> darEstadoEmpleadoId(int ID) {
		List<String> listaEmp = new ArrayList<String>();
		List<String> estadoEmplead = new ArrayList<String>();
    	try {			
			OracleCallableStatement oraCallStmt = null;
			OracleResultSet deptResultSet = null;
			
			oraCallStmt = (OracleCallableStatement) conn.prepareCall("{? = call demo.DEPTOSYEMPDISPO.GETEMPLOYEE_ID(?)}"
	            );
			
			oraCallStmt.registerOutParameter(1, OracleTypes.CURSOR);
			oraCallStmt.setInt(2, ID);
			oraCallStmt.execute();
			deptResultSet = (OracleResultSet) oraCallStmt.getCursor(1);//This is the cursor with the data.
			String sp = " ";
			while(deptResultSet.next())
			{
				
				//System.out.println(deptResultSet.getString("LAST_NAME"));
				String Resultado = deptResultSet.getString("STATUS_ID") ;
				if(Resultado=="")
					listaEmp.add("0");
				listaEmp.add(Resultado);
			}
			estadoEmplead = darEstadoEmpleadoDesc(listaEmp.get(0));
			oraCallStmt.close();
			deptResultSet.close();
		} catch (SQLException e) {
			//e.printStackTrace();
			System.out.println(e.getMessage());
		}
    	return estadoEmplead;
	}



}