package Gui;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import ConectorBD.ConexionBDOracle;

public class StartGui extends JFrame {

	private JPanel contentPane;

	private ConexionBDOracle conexionOracle;
	private JLabel lblEstadoConexion = new JLabel("Estado");
	private String userDb;
	

	private String passwordDb;
	private String ipOracle;
	private String autoConnect ;
	private String puerto;
	private String instancia;
	
	/* Creacion de las constantes */
	public static String ESTADO_CONEXION_ORACLE = "Conexion Oracle";
	public static String ESTADO_DESCONECTADO = "No hay conexion. Revisa IP, contrase�a, usuario, Conexiones ?";
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					//�com.jtattoo.plaf.acryl.AcrylLookAndFeel�
					//�com.jtattoo.plaf.noire.NoireLookAndFeel�
					//�com.jtattoo.plaf.aero.AeroLookAndFeel�
					//com.jtattoo.plaf.graphite.GraphiteLookAndFeel
					//com.jtattoo.plaf.smart.SmartLookAndFeel
					//com.jtattoo.plaf.aluminium.AluminiumLookAndFeel
					UIManager.setLookAndFeel("com.jtattoo.plaf.aluminium.AluminiumLookAndFeel"); 
					StartGui frame = new StartGui();
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
					frame.autoConnect();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public StartGui() {
		setResizable(false);
		setTitle("Demo PL/SQL");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 786, 537);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		mnArchivo.setFont(new Font("Segoe UI", Font.PLAIN, 20));
		menuBar.add(mnArchivo);
		
		JMenuItem mntmConectar = new JMenuItem("Conectar");
		mntmConectar.setFont(new Font("Segoe UI", Font.PLAIN, 16));
		mntmConectar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cargarConexiones();
				}
		});
		mnArchivo.add(mntmConectar);
		
		JMenuItem mntmCerrar = new JMenuItem("Cerrar");
		mntmCerrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		mntmCerrar.setFont(new Font("Segoe UI", Font.PLAIN, 16));
		mnArchivo.add(mntmCerrar);
		
		JMenu mnPreferencias = new JMenu("Preferencias");
		mnPreferencias.setFont(new Font("Segoe UI", Font.PLAIN, 20));
		menuBar.add(mnPreferencias);
		
		JMenuItem mntmCambiarIp = new JMenuItem("Configuraciones");
		mntmCambiarIp.setFont(new Font("Segoe UI", Font.PLAIN, 16));
		mntmCambiarIp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				HerramientasGui hGui = new HerramientasGui(this);
				hGui.linkStartGui(StartGui.this);
				hGui.setVisible(true);
				hGui.setLocationRelativeTo(null);
				hGui.asignarIP();
				hGui.AsignarEstadoAC(getAutoConnect());
				hGui.asignarInstancia(instancia);
				hGui.asignarPuerto(puerto);
				StartGui.this.setEnabled(false);
			}
		});
		mnPreferencias.add(mntmCambiarIp);
		
		JMenu mnEmpleado = new JMenu("Empleado");
		mnEmpleado.setFont(new Font("Segoe UI", Font.PLAIN, 20));
		menuBar.add(mnEmpleado);
		
		JMenuItem mntmUpdate = new JMenuItem("Actualizar Sueldo");
		mntmUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//SUELDO
				ActualizarSueldoEmpGui gui = new ActualizarSueldoEmpGui();
				gui.linkGui(StartGui.this);
				gui.setLocationRelativeTo(null);
				gui.setVisible(true);
				gui.actualizarDatos();
			}
		});
		
		JMenuItem mntmBuscar = new JMenuItem("Buscar");
		mntmBuscar.setFont(new Font("Segoe UI", Font.PLAIN, 16));
		mntmBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				BuscarEmpGui bEmpGui = new BuscarEmpGui(StartGui.this);
				bEmpGui.setVisible(true);
				bEmpGui.setLocationRelativeTo(null);
				StartGui.this.setEnabled(false);
				
			}
		});
		mnEmpleado.add(mntmBuscar);
		
		JMenuItem mntmInsert = new JMenuItem("Registrar");
		mntmInsert.setFont(new Font("Segoe UI", Font.PLAIN, 16));
		mntmInsert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				InsertarEmpGui iGui = new InsertarEmpGui();
				iGui.linkGUI(StartGui.this);
				iGui.setLocationRelativeTo(null);
				iGui.setVisible(true);
				iGui.darDepartments();
				iGui.darJobs();
			}
		});
		mnEmpleado.add(mntmInsert);
		mntmUpdate.setFont(new Font("Segoe UI", Font.PLAIN, 16));
		mnEmpleado.add(mntmUpdate);
		
		JMenuItem mntmEliminar = new JMenuItem("Cambiar Estado");
		mntmEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EstadoEmpleadoGui g = new EstadoEmpleadoGui();
				g.setVisible(true);
				g.setLocationRelativeTo(null);
				g.LinkGui(StartGui.this);
				g.actualizarDatos();
			}
		});
		mntmEliminar.setFont(new Font("Segoe UI", Font.PLAIN, 16));
		mnEmpleado.add(mntmEliminar);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblEstado = new JLabel("Estado: ");
		lblEstado.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblEstado.setBounds(12, 436, 51, 27);
		contentPane.add(lblEstado);
		
		
		lblEstadoConexion.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblEstadoConexion.setBounds(73, 436, 611, 27);
		contentPane.add(lblEstadoConexion);
		try {
			cargarLogin();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	public void cargarConexiones()
	{
		try {
			cargarLogin();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		crearConexionOracle();
		if(conexionOracle.testConecction())
		{
			lblEstadoConexion.setText(ESTADO_CONEXION_ORACLE);
		}
		else 
			lblEstadoConexion.setText(ESTADO_DESCONECTADO);
		
	}
	public void cargarLogin() throws IOException
	{
		BufferedReader br = null;
		try {
			
			FileReader file = new FileReader("Oracle.txt");
			
		 br = new BufferedReader(file);
		 
		        StringBuilder sb = new StringBuilder();
		        String line = br.readLine();
		        ArrayList<String> ips = new ArrayList<String>();

		        while (line != null) {
		            sb.append(line);
		            sb.append(System.lineSeparator());
		            ips.add(line);
		            line = br.readLine();
		            
		        }
/*
 * 					writer.println(oracle);
					writer.println(User);
					writer.println(password);
					writer.println(autoConnect);
					writer.print(instancia);
					writer.print(puerto);
 */
		        //String everything = sb.toString();
		        setIpOracle(ips.get(0).toString());
		        setUserDb(ips.get(1).toString());
		        setPasswordDb(ips.get(2).toString());
		        setAutoConnect(ips.get(3).toString());
		        setInstancia(ips.get(4).toString());
		        setPuerto(ips.get(5).toString());

		    }catch(NullPointerException ex)
		{
		    	System.out.println("Una de las IP no esta guardada");
		}
		catch (IndexOutOfBoundsException e) {
			// TODO: handle exception
			System.out.println("No existe el dato en el archivo");
		}
		
		finally {
			try {
				br.close();
			} catch (NullPointerException e2) {
				// TODO: handle exception
				System.out.println("Error desconocido");
			}
		        
		    }
	}
	public void autoConnect()
	{
		try {
			cargarLogin();

		if(autoConnect.contains("on"))
		{
			cargarConexiones();
		}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (NullPointerException e) {
			// TODO: handle exception
			System.out.println("Informacion perdida de la configuracion");
		}
	}
	
	public void crearConexionOracle()
	{
		conexionOracle = new ConexionBDOracle(ipOracle,userDb,passwordDb,instancia,puerto);
	}
	public void refrescarConexionOracle()
	{
		try {
			conexionOracle.getConnection().close();
			crearConexionOracle();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getIpOracle() {
		return ipOracle;
	}

	public void setIpOracle(String ipOracle) {
		this.ipOracle = ipOracle;
	}
	public String getUserDb() {
		return userDb;
	}

	public void setUserDb(String userDb) {
		this.userDb = userDb;
	}

	public String getPasswordDb() {
		return passwordDb;
	}

	public void setPasswordDb(String passwordDb) {
		this.passwordDb = passwordDb;
	}

	public ConexionBDOracle getConexionOracle() {
		return conexionOracle;
	}

	public void setConexionOracle(ConexionBDOracle conexionOracle) {
		this.conexionOracle = conexionOracle;
	}
	public String getAutoConnect() {
		return autoConnect;
	}

	public void setAutoConnect(String autoConnect) {
		this.autoConnect = autoConnect;
	}

	public String getPuerto() {
		return puerto;
	}

	public void setPuerto(String puerto) {
		this.puerto = puerto;
	}

	public String getInstancia() {
		return instancia;
	}

	public void setInstancia(String instancia) {
		this.instancia = instancia;
	}

	
}
