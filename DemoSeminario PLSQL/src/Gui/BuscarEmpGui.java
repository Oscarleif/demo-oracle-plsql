package Gui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import ConectorBD.ConexionBDOracle;
import Utils.StringMatcher;

public class BuscarEmpGui extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldNombre;
	private JLabel lblApellido;
	private JTextField textFieldApellido;
	private JRadioButton radioButtonApellido;
	private JLabel lblIdEmpleado;
	private JRadioButton radioButtonIDEmp;
	private JRadioButton radioButtonNombre = new JRadioButton("");
	private StartGui gui;
	private JTextField textFieldID;
	private JButton btnBuscar = new JButton("Buscar");
	private JTextField textFieldSueldoA;
	private JTextField textFieldSueldoB;
	private JLabel lblMenor;
	private JLabel lblMenor_1;
	private JRadioButton radioButtonSueldo = new JRadioButton("");
	/**
	 * Create the frame.
	 */
	public BuscarEmpGui(StartGui startGui) {
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				int codeKey = arg0.getKeyCode();
				if(codeKey == KeyEvent.VK_ESCAPE)
				{
					BuscarEmpGui.this.dispose();
				}
			}
		});
		setResizable(false);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane
						.showConfirmDialog(
								null,
								"�Estas seguro que deseas cerrar?",
								"Alerta", dialogButton);
				if (dialogResult == JOptionPane.YES_OPTION)
					{
					dispose();
					gui.setEnabled(true);
					gui.setVisible(true);
					} else if (dialogResult == JOptionPane.NO_OPTION)
					{
						System.out.println("Cancel the close window");
					}
				
			}
		});
		setTitle("Buscar empleado");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 515, 320);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNombre.setBounds(10, 30, 70, 14);
		contentPane.add(lblNombre);
		
		
		radioButtonNombre.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				radioButtonApellido.setSelected(false);
				radioButtonIDEmp.setSelected(false);
				radioButtonSueldo.setSelected(false);

				textFieldNombre.setEnabled(true);
				textFieldApellido.setEnabled(false);
				textFieldID.setEnabled(false);
				
				textFieldSueldoA.setEnabled(false);
				textFieldSueldoB.setEnabled(false);
			}
		});
		radioButtonNombre.setBounds(466, 26, 37, 23);
		contentPane.add(radioButtonNombre);
		
		textFieldNombre = new JTextField();
		textFieldNombre.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				searchMethod();
			}
		});
		textFieldNombre.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				radioButtonNombre.setSelected(true);
				radioButtonApellido.setSelected(false);
				radioButtonIDEmp.setSelected(false);
				radioButtonSueldo.setSelected(false);
				
				textFieldApellido.setEnabled(false);
				textFieldID.setEnabled(false);
				textFieldNombre.setEnabled(true);
				textFieldSueldoA.setEnabled(false);
				textFieldSueldoB.setEnabled(false);
			}
		});
		textFieldNombre.setBounds(107, 27, 353, 20);
		contentPane.add(textFieldNombre);
		textFieldNombre.setColumns(10);
		
		lblApellido = new JLabel("Apellido:");
		lblApellido.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblApellido.setBounds(10, 91, 70, 17);
		contentPane.add(lblApellido);
		
		textFieldApellido = new JTextField();
		textFieldApellido.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				searchMethod();
			}
			
		});
		textFieldApellido.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				radioButtonNombre.setSelected(false);
				radioButtonApellido.setSelected(true);
				radioButtonIDEmp.setSelected(false);
				radioButtonSueldo.setSelected(false);
				
				textFieldNombre.setEnabled(false);
				textFieldApellido.setEnabled(true);
				textFieldID.setEnabled(false);
				textFieldSueldoA.setEnabled(false);
				textFieldSueldoB.setEnabled(false);
			}
		});
		textFieldApellido.setColumns(10);
		textFieldApellido.setBounds(107, 89, 353, 20);
		contentPane.add(textFieldApellido);
		
		radioButtonApellido = new JRadioButton("");
		radioButtonApellido.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				radioButtonNombre.setSelected(false);
				radioButtonIDEmp.setSelected(false);
				radioButtonSueldo.setSelected(false);
				
				textFieldApellido.setEnabled(true);
				textFieldNombre.setEnabled(false);
				textFieldID.setEnabled(false);
				
				textFieldSueldoA.setEnabled(false);
				textFieldSueldoB.setEnabled(false);
			}
		});
		radioButtonApellido.setBounds(466, 88, 37, 23);
		contentPane.add(radioButtonApellido);
		
		lblIdEmpleado = new JLabel("ID empleado:");
		lblIdEmpleado.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblIdEmpleado.setBounds(10, 148, 101, 17);
		contentPane.add(lblIdEmpleado);
		
		radioButtonIDEmp = new JRadioButton("");
		radioButtonIDEmp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				radioButtonApellido.setSelected(false);
				radioButtonNombre.setSelected(false);
				radioButtonSueldo.setSelected(false);
				
				textFieldNombre.setEnabled(false);
				textFieldApellido.setEnabled(false);
				textFieldID.setEnabled(true);
				
				textFieldSueldoA.setEnabled(false);
				textFieldSueldoB.setEnabled(false);
			}
		});
		radioButtonIDEmp.setBounds(466, 145, 37, 23);
		contentPane.add(radioButtonIDEmp);
		
		
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				searchMethod();
			}
		});
		btnBuscar.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnBuscar.setBounds(10, 253, 111, 27);
		contentPane.add(btnBuscar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane
						.showConfirmDialog(
								null,
								"�Estas seguro que deseas cerrar?",
								"Alerta", dialogButton);
				if (dialogResult == JOptionPane.YES_OPTION)
					{
					dispose();
					gui.setEnabled(true);
					gui.setVisible(true);
					} else if (dialogResult == JOptionPane.NO_OPTION)
					{
						System.out.println("Cancel the close window");
					}
				
			}
		});
		btnCancelar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnCancelar.setBounds(392, 253, 111, 27);
		contentPane.add(btnCancelar);
		
		textFieldID = new JTextField();
		textFieldID.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				searchMethod();
			}
		});
		textFieldID.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				radioButtonNombre.setSelected(false);
				radioButtonApellido.setSelected(false);
				radioButtonIDEmp.setSelected(true);
				radioButtonSueldo.setSelected(false);
				
				textFieldNombre.setEnabled(false);
				textFieldApellido.setEnabled(false);
				textFieldID.setEnabled(true);
				textFieldSueldoA.setEnabled(false);
				textFieldSueldoB.setEnabled(false);
			}
		});
		textFieldID.setColumns(10);
		textFieldID.setBounds(107, 146, 353, 20);
		contentPane.add(textFieldID);
		
		JLabel lblValor = new JLabel("Sueldo:");
		lblValor.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblValor.setBounds(10, 213, 92, 17);
		contentPane.add(lblValor);
		
		textFieldSueldoA = new JTextField();
		textFieldSueldoA.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
											
				radioButtonSueldo.setSelected(true);
				radioButtonApellido.setSelected(false);
				radioButtonIDEmp.setSelected(false);
				radioButtonNombre.setSelected(false);
				
				textFieldSueldoA.setEnabled(true);
				textFieldSueldoB.setEnabled(true);
				
				textFieldApellido.setEnabled(false);
				textFieldID.setEnabled(false);
				textFieldNombre.setEnabled(false);

			}
		});
		textFieldSueldoA.setColumns(10);
		textFieldSueldoA.setBounds(176, 213, 86, 17);
		contentPane.add(textFieldSueldoA);
		
		textFieldSueldoB = new JTextField();
		textFieldSueldoB.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				radioButtonSueldo.setSelected(true);
				
				radioButtonApellido.setSelected(false);
				radioButtonIDEmp.setSelected(false);
				radioButtonNombre.setSelected(false);
				
				textFieldSueldoA.setEnabled(true);
				textFieldSueldoB.setEnabled(true);
				
				textFieldApellido.setEnabled(false);
				textFieldID.setEnabled(false);
				textFieldNombre.setEnabled(false);
			}
		});
		textFieldSueldoB.setColumns(10);
		textFieldSueldoB.setBounds(374, 213, 86, 17);
		contentPane.add(textFieldSueldoB);
		radioButtonSueldo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				radioButtonNombre.setSelected(false);
				radioButtonApellido.setSelected(false);
				radioButtonIDEmp.setSelected(false);
				
				textFieldNombre.setEnabled(false);
				textFieldApellido.setEnabled(false);
				textFieldID.setEnabled(false);
				textFieldSueldoA.setEnabled(true);
				textFieldSueldoB.setEnabled(true);
			}
		});
		
		
		radioButtonSueldo.setBounds(466, 213, 37, 17);
		contentPane.add(radioButtonSueldo);
		
		lblMenor = new JLabel("entre mayor:");
		lblMenor.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblMenor.setBounds(272, 213, 92, 17);
		contentPane.add(lblMenor);
		
		lblMenor_1 = new JLabel("menor:");
		lblMenor_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblMenor_1.setBounds(107, 213, 59, 17);
		contentPane.add(lblMenor_1);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 509, 21);
		contentPane.add(menuBar);
		
		JMenu mnAyuda = new JMenu("Ayuda");
		menuBar.add(mnAyuda);
		
		this.gui = startGui;
	}
	private void searchMethod()
	{
		ConexionBDOracle co = gui.getConexionOracle();
		//co.darListaEmpleados("");//Works
		if(radioButtonApellido.isSelected())
		{
			String apellido = textFieldApellido.getText();
			boolean isText = new StringMatcher().isCharacters(apellido);
			if(isText)//Si es un texto funciona el metodo
			{
			java.util.List<String> lista = co.darListaEmpleadosApellido(apellido);
			ResultadosBusquedaGui rB = new ResultadosBusquedaGui();
			rB.setResult(lista);
			rB.setFatherGui(BuscarEmpGui.this);
			rB.setLocationRelativeTo(null);
			rB.setVisible(true);
			this.setEnabled(false);
			}
			else 
				JOptionPane.showMessageDialog(this,"Ingresa por favor un apellido para buscar","Campo no valido, solo texto.",JOptionPane.ERROR_MESSAGE);
		}
		else if(radioButtonNombre.isSelected())
		{
			String nombre = textFieldNombre.getText();
			boolean isText = new StringMatcher().isCharacters(nombre);
			if(isText)
			{
			java.util.List<String> lista = co.darListaEmpleadosNombre(nombre);
			ResultadosBusquedaGui rB = new ResultadosBusquedaGui();
			rB.setResult(lista);
			rB.setFatherGui(BuscarEmpGui.this);
			rB.setLocationRelativeTo(null);
			rB.setVisible(true);
			this.setEnabled(false);
			}
			else
				JOptionPane.showMessageDialog(this,"Ingresa por favor un nombre para buscar","Campo no valido, solo texto.",JOptionPane.ERROR_MESSAGE);
		}
		else if(radioButtonIDEmp.isSelected())
		{
			try
			{
			int id = Integer.parseInt(textFieldID.getText());
			java.util.List<String> lista = co.darListaEmpleadosId(id);
			ResultadosBusquedaGui rB = new ResultadosBusquedaGui();
			rB.setResult(lista);
			rB.setFatherGui(BuscarEmpGui.this);
			rB.setLocationRelativeTo(null);
			rB.setVisible(true);
			this.setEnabled(false);
			}
			catch(NumberFormatException error)
			{
				JOptionPane.showMessageDialog(this,"ID es incorrecto, escribe un valor entero.","Valor numerico",JOptionPane.ERROR_MESSAGE);
			}
		}
		else if (radioButtonSueldo.isEnabled())
		{
			StringMatcher s = new StringMatcher();
			boolean esValorMenor = s.isNumber(textFieldSueldoA.getText());
			boolean esValorMayor = s.isNumber(textFieldSueldoB.getText());
			if(esValorMayor && esValorMayor)
			{
				int valorMenor = Integer.parseInt(textFieldSueldoA.getText());
				int valorMayor = Integer.parseInt(textFieldSueldoB.getText());
				if(valorMayor > valorMenor)
				{
					java.util.List<String> lista = co.darListaEmpleadosPorSueldo(valorMenor, valorMayor);
					ResultadosBusquedaGui rB = new ResultadosBusquedaGui();
					rB.setResult(lista);
					rB.setFatherGui(BuscarEmpGui.this);
					rB.setLocationRelativeTo(null);
					rB.setVisible(true);
					this.setEnabled(false);
				}
				else 
					JOptionPane.showMessageDialog(this,"Escribe el sueldo menor en el campor izquierdo, el valor mayor en el campo derecho.","Comparacion incorrecta",JOptionPane.ERROR_MESSAGE);	
			
			}
			else 
				JOptionPane.showMessageDialog(this,"Por favor escribe un valor numerico.","Valor numerico",JOptionPane.ERROR_MESSAGE);
			
		}
	}
}
