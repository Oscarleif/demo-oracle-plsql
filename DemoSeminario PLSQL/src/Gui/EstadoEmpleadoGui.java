package Gui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import Utils.StringMatcher;
import ConectorBD.ConexionBDOracle;

public class EstadoEmpleadoGui extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldEmpleado;
	private JTextField textFieldEstadoDescActual;
	private StartGui GUI;
	private int estadoEmpleadoActual;//Esta variable es usada para asignar el valor del estado de un empleado en la base de datos.
	private int estadoEmpleadoNuevo;
	//No se muestra por GUI por ser muy incomodo para la vista. supongo
	private JComboBox comboBoxIDEmp = new JComboBox();
	private JComboBox comboBox_EstadoDesc = new JComboBox();
	
	/**
	 * Create the frame.
	 */
	public EstadoEmpleadoGui() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane
						.showConfirmDialog(
								null,
								"�Estas seguro que deseas cerrar?",
								"Alerta", dialogButton);
				if (dialogResult == JOptionPane.YES_OPTION)
					{
						dispose();
						GUI.setEnabled(true);
						GUI.setVisible(true);
					} else if (dialogResult == JOptionPane.NO_OPTION)
					{
						System.out.println("Cancel the close window");
					}
			}
		});
		setResizable(false);
		setTitle("Estado empleado");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 632, 388);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		mnArchivo.setFont(new Font("Segoe UI", Font.PLAIN, 16));
		menuBar.add(mnArchivo);
		
		JMenuItem mntmCerrar = new JMenuItem("Cerrar");
		mntmCerrar.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		mnArchivo.add(mntmCerrar);
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblIdentifiacionEmpleado = new JLabel("Nuevo estado:");
		lblIdentifiacionEmpleado.setBounds(20, 211, 176, 27);
		lblIdentifiacionEmpleado.setFont(new Font("Tahoma", Font.PLAIN, 16));
		contentPane.add(lblIdentifiacionEmpleado);
		
		JLabel lblEmpleadoi = new JLabel("Empleado:");
		lblEmpleadoi.setBounds(20, 28, 176, 25);
		lblEmpleadoi.setFont(new Font("Tahoma", Font.PLAIN, 16));
		contentPane.add(lblEmpleadoi);
		comboBoxIDEmp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Get stado, luego asigna el estado en la base de datos.
				int id = Integer.parseInt(comboBoxIDEmp.getSelectedItem().toString());
				List<String> listaEmpList = GUI.getConexionOracle().darListaEmpleadosId(id);
				String s = listaEmpList.get(0).substring(5);
				textFieldEmpleado.setText(s);
				try
				
				{
				String iden = GUI.getConexionOracle().darEstadoEmpleadoIdByEmpID(id).get(0);
				
				System.out.println("STATUS_ID estadoEmpleado por Id = "+iden);//Status_ID del empleado seleccionado, puede regresar NULL;
				
				List<String> descripcion = GUI.getConexionOracle().darEstadoEmpleadoDesc(iden);

				textFieldEstadoDescActual.setText(descripcion.get(0));
				}
				catch(IndexOutOfBoundsException e)
				{
					System.out.println("No hay descripccion");
				}
				int ID = Integer.parseInt(comboBoxIDEmp.getSelectedItem().toString());
				List<String> estado = GUI.getConexionOracle().darEstadoEmpleadoId(ID);//retorna un estado
				String est;
				try
				{
					 est = estado.get(0);
				}
				catch(IndexOutOfBoundsException e)
				{
					est = "No hay estado";
				}
				textFieldEstadoDescActual.setText(est);
			}
		});
		
		
		comboBoxIDEmp.setFont(new Font("Tahoma", Font.PLAIN, 16));
		comboBoxIDEmp.setBounds(237, 138, 372, 27);
		contentPane.add(comboBoxIDEmp);
		
		textFieldEmpleado = new JTextField();
		textFieldEmpleado.setEditable(false);
		textFieldEmpleado.setFont(new Font("Tahoma", Font.PLAIN, 16));
		textFieldEmpleado.setBounds(237, 28, 372, 25);
		contentPane.add(textFieldEmpleado);
		textFieldEmpleado.setColumns(10);
		
		JLabel lblEstadoActual = new JLabel("Estado actual: ");
		lblEstadoActual.setBounds(20, 79, 176, 27);
		lblEstadoActual.setFont(new Font("Tahoma", Font.PLAIN, 16));
		contentPane.add(lblEstadoActual);
		
		
		comboBox_EstadoDesc.setBounds(237, 213, 372, 27);
		contentPane.add(comboBox_EstadoDesc);
		
		JLabel lblRegistrarCambios = new JLabel("Registrar Cambios");
		lblRegistrarCambios.setHorizontalAlignment(SwingConstants.CENTER);
		lblRegistrarCambios.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblRegistrarCambios.setBounds(31, 251, 176, 27);
		contentPane.add(lblRegistrarCambios);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				boolean isId = new StringMatcher().isNumber(comboBoxIDEmp.getSelectedItem().toString());
				String descEstado= comboBox_EstadoDesc.getSelectedItem().toString();
				//necesito la Id basado la descripccion.
				
				List<String> i = GUI.getConexionOracle().darIdEstadoEmpleadoByDesc(comboBox_EstadoDesc.getSelectedItem().toString());
				int newEstatus = Integer.parseInt(i.get(0));
				if(isId)
				{
					boolean operacion = GUI.getConexionOracle().actualizarEstadoEmpleado(Integer.parseInt(comboBoxIDEmp.getSelectedItem().toString() ),newEstatus);
					if(operacion)
					{
						JOptionPane.showMessageDialog(null, "El estado del empleado ha sido actualizado");
						EstadoEmpleadoGui.this.GUI.setEnabled(true);
						EstadoEmpleadoGui.this.GUI.setVisible(true);
						EstadoEmpleadoGui.this.dispose();
					}
					else 
					{
						JOptionPane.showMessageDialog(null, "Se encontro un error");
					}
					
				}
				actualizarDatos();
			}
		});
		btnAceptar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnAceptar.setBounds(67, 289, 89, 23);
		contentPane.add(btnAceptar);
		
		JLabel lblCancelarCambios = new JLabel("Cancelar cambios");
		lblCancelarCambios.setHorizontalAlignment(SwingConstants.CENTER);
		lblCancelarCambios.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblCancelarCambios.setBounds(433, 251, 176, 27);
		contentPane.add(lblCancelarCambios);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane
						.showConfirmDialog(
								null,
								"�Estas seguro que deseas cerrar?",
								"Alerta", dialogButton);
				if (dialogResult == JOptionPane.YES_OPTION)
					{
						dispose();
						GUI.setEnabled(true);
						GUI.setVisible(true);
					} else if (dialogResult == JOptionPane.NO_OPTION)
					{
						System.out.println("Cancel the close window");
					}
			}
		});
		btnCancelar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnCancelar.setBounds(469, 289, 89, 23);
		contentPane.add(btnCancelar);
		
		textFieldEstadoDescActual = new JTextField();
		textFieldEstadoDescActual.setEditable(false);
		textFieldEstadoDescActual.setColumns(10);
		textFieldEstadoDescActual.setBounds(237, 81, 372, 27);
		contentPane.add(textFieldEstadoDescActual);
		
		JLabel lblIdEmpleado = new JLabel("I.D. Empleado:");
		lblIdEmpleado.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblIdEmpleado.setBounds(20, 138, 176, 27);
		contentPane.add(lblIdEmpleado);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Datos actuales", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 17, 606, 164);
		contentPane.add(panel);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "Actualizar estado", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(10, 192, 606, 132);
		contentPane.add(panel_1);
	}

	public void LinkGui(StartGui startGui) {
		// TODO Auto-generated method stub
		this.GUI = startGui;
	}
	public void actualizarDatos() {
		ConexionBDOracle o = GUI.getConexionOracle();
		List<String> listaEmpleados = o.darListaEmpleadosId();
		
		for (String string : listaEmpleados) {
			comboBoxIDEmp.addItem(string);
		}
		//actualiza los estados del comboBox Estado, se necesitan procedimientos para esto.
		List<String> listaEstadoDesc =o.darListaEstadoEmpleadoDesc();
		for (String string : listaEstadoDesc) {
			comboBox_EstadoDesc.addItem(string);
		}
		
	}
}
