package Gui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import ConectorBD.ConexionBDOracle;
import Utils.StringMatcher;

public class InsertarEmpGui extends JFrame {

	private StartGui gui;
	private JPanel contentPane;
	private JTextField textField_ID;
	private JTextField textField_FirstName;
	private JTextField textField_LastName;
	private JTextField textField_MI;
	private JTextField textField_JobId;
	private JTextField textField_ManagerID;
	private JTextField textField_Salary;
	private JTextField textField_Commision;
	private JTextField textField_DepartmentID;
	
	private JComboBox comboBoxDepartments = new JComboBox();
	private JComboBox comboBoxJob = new JComboBox();
	private ResultadosBusquedaGui resultadosBusqueda;
	/**
	 * Create the frame.
	 */
	public InsertarEmpGui() {
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent ke) {
				
				if(ke.getKeyCode() == ke.VK_ESCAPE) {
				      System.out.println("escaped ?");
				      InsertarEmpGui.this.dispose();
				      } 
				     else {
				      System.out.println("not escaped");
				      }
				     
			}
		});
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane
						.showConfirmDialog(
								null,
								"�Estas seguro que deseas cerrar?",
								"Alerta", dialogButton);
				if (dialogResult == JOptionPane.YES_OPTION)
					{
						dispose();
						gui.setEnabled(true);
						gui.setVisible(true);
					} else if (dialogResult == JOptionPane.NO_OPTION)
					{
						System.out.println("Cancel the close window");
					}
			}
		});
		setTitle("Insertar Empleado");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 515, 639);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		mnArchivo.setFont(new Font("Segoe UI", Font.PLAIN, 15));
		menuBar.add(mnArchivo);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblId = new JLabel("I.D :");
		lblId.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblId.setBounds(36, 22, 136, 25);
		contentPane.add(lblId);
		
		JLabel lblFirstName = new JLabel("First name");
		lblFirstName.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblFirstName.setBounds(36, 69, 136, 25);
		contentPane.add(lblFirstName);
		
		JLabel lblLastName = new JLabel("Last name");
		lblLastName.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblLastName.setBounds(36, 116, 136, 25);
		contentPane.add(lblLastName);
		
		JLabel lblMiddleInitial = new JLabel("Middle Initial");
		lblMiddleInitial.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblMiddleInitial.setBounds(36, 163, 136, 25);
		contentPane.add(lblMiddleInitial);
		
		JLabel lblJobId = new JLabel("Job ID");
		lblJobId.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblJobId.setBounds(36, 210, 136, 25);
		contentPane.add(lblJobId);
		
		JLabel lblManagerId = new JLabel("Manager ID");
		lblManagerId.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblManagerId.setBounds(36, 257, 136, 25);
		contentPane.add(lblManagerId);
		
		JLabel lblSalary = new JLabel("Salary");
		lblSalary.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblSalary.setBounds(36, 304, 136, 25);
		contentPane.add(lblSalary);
		
		JLabel lblCommission = new JLabel("Commission");
		lblCommission.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblCommission.setBounds(36, 351, 136, 25);
		contentPane.add(lblCommission);
		
		JLabel lblDepartmentId = new JLabel("Department ID");
		lblDepartmentId.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblDepartmentId.setBounds(36, 398, 136, 25);
		contentPane.add(lblDepartmentId);
		
		textField_ID = new JTextField();
		textField_ID.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_ID.setBounds(182, 22, 305, 24);
		contentPane.add(textField_ID);
		textField_ID.setColumns(10);
		
		textField_FirstName = new JTextField();
		textField_FirstName.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_FirstName.setColumns(10);
		textField_FirstName.setBounds(182, 69, 305, 25);
		contentPane.add(textField_FirstName);
		
		textField_LastName = new JTextField();
		textField_LastName.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_LastName.setColumns(10);
		textField_LastName.setBounds(182, 116, 305, 25);
		contentPane.add(textField_LastName);
		
		textField_MI = new JTextField();
		textField_MI.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_MI.setColumns(10);
		textField_MI.setBounds(182, 163, 305, 24);
		contentPane.add(textField_MI);
		
		textField_JobId = new JTextField();
		textField_JobId.setEditable(false);
		textField_JobId.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_JobId.setColumns(10);
		textField_JobId.setBounds(182, 210, 305, 25);
		contentPane.add(textField_JobId);
		
		textField_ManagerID = new JTextField();
		textField_ManagerID.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_ManagerID.setColumns(10);
		textField_ManagerID.setBounds(182, 257, 305, 25);
		contentPane.add(textField_ManagerID);
		
		textField_Salary = new JTextField();
		textField_Salary.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_Salary.setColumns(10);
		textField_Salary.setBounds(182, 304, 305, 24);
		contentPane.add(textField_Salary);
		
		textField_Commision = new JTextField();
		textField_Commision.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_Commision.setColumns(10);
		textField_Commision.setBounds(182, 351, 305, 25);
		contentPane.add(textField_Commision);
		
		textField_DepartmentID = new JTextField();
		textField_DepartmentID.setEditable(false);
		textField_DepartmentID.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_DepartmentID.setColumns(10);
		textField_DepartmentID.setBounds(182, 398, 305, 25);
		contentPane.add(textField_DepartmentID);
		
		JLabel lblDepartments = new JLabel("Departments");
		lblDepartments.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblDepartments.setBounds(36, 445, 136, 25);
		contentPane.add(lblDepartments);
		
		comboBoxDepartments = new JComboBox();
		comboBoxDepartments.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("The action event job is working");
				ConexionBDOracle oracle = gui.getConexionOracle();
				int id=oracle.returnDepartmentId(comboBoxDepartments.getSelectedItem().toString());
				textField_DepartmentID.setText( Integer.toString(id));
				System.out.println(id);
			}
		});
		comboBoxDepartments.setBounds(182, 447, 303, 25);
		contentPane.add(comboBoxDepartments);
		
		JLabel lblEmployTitle = new JLabel("Job Title");
		lblEmployTitle.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblEmployTitle.setBounds(36, 494, 136, 25);
		contentPane.add(lblEmployTitle);
		comboBoxJob.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("The action event job is working");
				ConexionBDOracle oracle = gui.getConexionOracle();
				int id=oracle.returnJobId(comboBoxJob.getSelectedItem().toString());
				textField_JobId.setText( Integer.toString(id));
				System.out.println(id);
			}
		});
		
		
		comboBoxJob.setBounds(182, 496, 303, 25);
		contentPane.add(comboBoxJob);
		
		JButton btnRegister = new JButton("Registrar");
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Evento Registrar Cliente
				StringMatcher m = new StringMatcher();
				
				boolean isId = m.isNumber(textField_ID.getText());
				boolean isSalary =m.isNumber(textField_Salary.getText());
				boolean isCommissionm = m.isNumber(textField_Commision.getText());
				boolean isName = m.isCharacters(textField_FirstName.getText());
				boolean isInitial = m.isCharacters(textField_MI.getText());
				boolean isLastName = m.isCharacters(textField_LastName.getText());
				
				int jobId ; 
				
				jobId = Integer.parseInt(textField_JobId.getText());
				
				boolean isManagerId = m.isNumber(textField_JobId.getText());
				boolean register=false;
				if(isId && isSalary &&isCommissionm &&isName&&isInitial&&isLastName)
				{
					int id = Integer.parseInt(textField_ID.getText());
					String name = textField_FirstName.getText();
					String lastName = textField_LastName.getText();
					String MiddleInitial = textField_MI.getText();
					
					int managerId = Integer.parseInt(textField_ManagerID.getText());
					int salary = Integer.parseInt(textField_Salary.getText());
					int commission = Integer.parseInt(textField_Commision.getText());
					
					int departmentId = Integer.parseInt(textField_DepartmentID.getText());
					
					ConexionBDOracle oracle = gui.getConexionOracle();
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//This one looks amazing
					Date date = new Date();
					
					java.sql.Date sqlDate = new java.sql.Date(date.getTime());
					try {
					register = oracle.registrarEmpleado(id, name, lastName, MiddleInitial, jobId, managerId, sqlDate, salary, commission, departmentId);
					if(register)
					{
						oracle.getConnection().commit();
						JOptionPane.showMessageDialog(InsertarEmpGui.this, "Se registro el usuario de forma satisfactoria");
						InsertarEmpGui.this.dispose();
						InsertarEmpGui.this.gui.setEnabled(true);
						InsertarEmpGui.this.gui.setVisible(true);
					}
					else if(!register)
					{
						JOptionPane.showMessageDialog(new JFrame(), "El usuario con esa identificacion ya existe.", "Error Database",JOptionPane.ERROR_MESSAGE);
					}
						
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
				else 
					JOptionPane.showMessageDialog(InsertarEmpGui.this, "Por favor revisalos campos");
				//JOptionPane.showMessageDialog(InsertarEmpGui.this, "Empleado registrado");
			}
		});
		btnRegister.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnRegister.setBounds(36, 541, 105, 27);
		contentPane.add(btnRegister);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane
						.showConfirmDialog(
								null,
								"�Estas seguro que deseas cerrar?",
								"Alerta", dialogButton);
				if (dialogResult == JOptionPane.YES_OPTION)
					{
					dispose();
					gui.setEnabled(true);
					gui.setVisible(true);
					} else if (dialogResult == JOptionPane.NO_OPTION)
					{
						System.out.println("Cancel the close window");
					}
				
			}
		});
		btnCancelar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnCancelar.setBounds(384, 541, 105, 25);
		contentPane.add(btnCancelar);
	}
	
	public void linkGUI(StartGui linkGui)
	{
		this.gui=linkGui;
		gui.setEnabled(false);
	}
	public void darDepartments()
	{
		ConexionBDOracle oracle = gui.getConexionOracle();
		java.util.List<String> listaDep;
		String function = "DEPTOSYEMPDISPO.GETDEPTOS";
		
		listaDep = (java.util.List<String>) gui.getConexionOracle().ListDarDep(function);
		
		for (String string : listaDep) {
			comboBoxDepartments.addItem(string);
		}
				
	}

	public void darJobs() 
	{
		ConexionBDOracle oracle = gui.getConexionOracle();
		java.util.List<String> listJobs = gui.getConexionOracle().listAvaJobs(
				"Procedure is inside this method");

		for (String string : listJobs) {
			comboBoxJob.addItem(string);
		}
	}
}
