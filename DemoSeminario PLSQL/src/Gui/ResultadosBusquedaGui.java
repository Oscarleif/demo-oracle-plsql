package Gui;

import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

public class ResultadosBusquedaGui extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JList list;
	private DefaultListModel model;
	private BuscarEmpGui GUI;
	/**
	 * Create the frame.
	 */
	public ResultadosBusquedaGui() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane
						.showConfirmDialog(
								null,
								"�Estas seguro que deseas cerrar?",
								"Alerta", dialogButton);
				if (dialogResult == JOptionPane.YES_OPTION)
					{
						dispose();
						GUI.setEnabled(true);
						GUI.setVisible(true);
					} else if (dialogResult == JOptionPane.NO_OPTION)
					{
						System.out.println("Cancel the close window");
					}
			}
			
		});
		setTitle("Resultados Busqueda");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblResutadosDeBusqueda = new JLabel("Resutados de Busqueda.");
		lblResutadosDeBusqueda.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblResutadosDeBusqueda.setBounds(109, 31, 181, 14);
		contentPane.add(lblResutadosDeBusqueda);
		
		
	}
	
	public void setResult(java.util.List aList) 
	{
		model = new DefaultListModel();
		list = new JList(model);
		JScrollPane pane = new JScrollPane(list);
		
		for (Object object : aList) {
			model.addElement(object);
		}
		
		
		
		pane.setBounds(10, 54, 414, 196);
		contentPane.add(pane);
	}

	public void setFatherGui(BuscarEmpGui buscarEmpGui) {
		// TODO Auto-generated method stub
		this.GUI = buscarEmpGui;
		
	}
}
