package Gui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class HerramientasGui extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private StartGui gui;
	private JTextField textFieldOracle;
	private JTextField textFieldUsuario;
	private JPasswordField passwordField;
	private JCheckBox chckbxAutoConexion = new JCheckBox("auto conexion");
	private JTextField textFieldInstacia;
	private JTextField textFieldPuerto;
	/**
	 * Create the frame.
	 * 
	 * @param actionListener
	 */
	public HerramientasGui(ActionListener actionListener) {
		setResizable(false);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane
						.showConfirmDialog(
								null,
								"�Estas seguro que deseas cerrar?",
								"Alerta", dialogButton);
				if (dialogResult == JOptionPane.YES_OPTION)
					{
						dispose();
						gui.setEnabled(true);gui.setVisible(true);
					} else if (dialogResult == JOptionPane.NO_OPTION)
					{
						System.out.println("Cancel the close window");
					}
			}
		});
		setTitle("Herramientas");
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 548, 362);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnArchivo = new JMenu("Archivo");
		mnArchivo.setFont(new Font("Segoe UI", Font.PLAIN, 16));
		menuBar.add(mnArchivo);

		JMenuItem mntmGuardar = new JMenuItem("Guardar");
		mnArchivo.add(mntmGuardar);

		JMenuItem mntmSalir = new JMenuItem("Salir");
		mnArchivo.add(mntmSalir);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblDireccionIpOracle = new JLabel("Asigna la IP de Oracle:");
		lblDireccionIpOracle.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblDireccionIpOracle.setBounds(10, 44, 167, 17);
		contentPane.add(lblDireccionIpOracle);

		textFieldOracle = new JTextField();
		textFieldOracle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				guardarDatos();
				gui.setEnabled(true);gui.setVisible(true);
				dispose();
			}
		});
		textFieldOracle.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textFieldOracle.setBounds(189, 42, 337, 20);
		contentPane.add(textFieldOracle);
		textFieldOracle.setColumns(10);

		JButton btnNewButton = new JButton("Guardar");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String oracle = textFieldOracle.getText();
				String User = textFieldUsuario.getText();
				char[] password = passwordField.getPassword();
				String instancia = textFieldInstacia.getText();
				String puerto = textFieldPuerto.getText();
				String autoConnect = "off";
				//gui.setIpOracle(oracle);
				if(chckbxAutoConexion.isSelected())
				{
					autoConnect = "on";
				}
				
				try {
					PrintWriter writer=new PrintWriter("Oracle.txt","UTF-8");
					writer.println(oracle);
					writer.println(User);
					writer.println(password);
					writer.println(autoConnect);
					writer.println(instancia);
					writer.println(puerto);
					writer.close();
					gui.setEnabled(true);gui.setVisible(true);gui.setVisible(true);
					dispose();
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		btnNewButton.setBounds(10, 264, 89, 23);
		contentPane.add(btnNewButton);

		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				gui.setEnabled(true);gui.setVisible(true);
				dispose();
			}
		});
		btnCancelar.setBounds(437, 264, 89, 23);
		contentPane.add(btnCancelar);
		
		JLabel lblAutoConectar = new JLabel("Auto conectar:");
		lblAutoConectar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblAutoConectar.setBounds(10, 225, 167, 28);
		contentPane.add(lblAutoConectar);
		
		
		chckbxAutoConexion.setFont(new Font("Tahoma", Font.PLAIN, 15));
		chckbxAutoConexion.setBounds(189, 230, 337, 23);
		contentPane.add(chckbxAutoConexion);
		
		JLabel lblUsuario = new JLabel("Usuario:");
		lblUsuario.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblUsuario.setBounds(10, 87, 167, 17);
		contentPane.add(lblUsuario);
		
		JLabel lblContrasea = new JLabel("Contrase\u00F1a: ");
		lblContrasea.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblContrasea.setBounds(10, 124, 167, 17);
		contentPane.add(lblContrasea);
		
		textFieldUsuario = new JTextField();
		textFieldUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				guardarDatos();
				gui.setEnabled(true);gui.setVisible(true);
				dispose();
			}
		});
		textFieldUsuario.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textFieldUsuario.setColumns(10);
		textFieldUsuario.setBounds(189, 87, 337, 20);
		contentPane.add(textFieldUsuario);
		
		passwordField = new JPasswordField();
		passwordField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				guardarDatos();
				gui.setEnabled(true);gui.setVisible(true);
				dispose();
			}
		});
		passwordField.setBounds(189, 124, 337, 20);
		contentPane.add(passwordField);
		
		JLabel lblInstancia = new JLabel("Instancia:");
		lblInstancia.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblInstancia.setBounds(10, 165, 167, 17);
		contentPane.add(lblInstancia);
		
		textFieldInstacia = new JTextField();
		textFieldInstacia.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textFieldInstacia.setColumns(10);
		textFieldInstacia.setBounds(189, 165, 337, 20);
		contentPane.add(textFieldInstacia);
		
		JLabel lblPuerto = new JLabel("Puerto:");
		lblPuerto.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblPuerto.setBounds(10, 196, 167, 17);
		contentPane.add(lblPuerto);
		
		textFieldPuerto = new JTextField();
		textFieldPuerto.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textFieldPuerto.setColumns(10);
		textFieldPuerto.setBounds(189, 196, 337, 20);
		contentPane.add(textFieldPuerto);
		 
		
	}

	public void asignarIP() {
		// TODO Auto-generated method stub
		try
		{
		textFieldOracle.setText(gui.getIpOracle());
		textFieldUsuario.setText(gui.getUserDb());
		passwordField.setText(gui.getPasswordDb());
		}
		catch(NullPointerException error)
		{
			textFieldOracle.setText("No hay Direccion IP");
		}
	}

	public void linkStartGui(StartGui gui) {
		this.gui = gui;
	}
	public void cargarIP()
	{
		
		
	}
	
	public void guardarDatos()
	{
		String oracle = textFieldOracle.getText();
		String User = textFieldUsuario.getText();
		char[] password = passwordField.getPassword();
		String autoConnect = "off";
		//gui.setIpOracle(oracle);
		if(chckbxAutoConexion.isSelected())
		{
			autoConnect = "on";
		}
		
		try {
			PrintWriter writer=new PrintWriter("Oracle.txt","UTF-8");
			writer.println(oracle);
			writer.println(User);
			writer.println(password);
			writer.println(autoConnect);
			writer.close();
			gui.setEnabled(true);gui.setVisible(true);
			dispose();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public void AsignarEstadoAC(String autoConnect) {
		// TODO Auto-generated method stub
		if(autoConnect.contains("on"))
		{
			chckbxAutoConexion.setSelected(true);
		}
		else
			chckbxAutoConexion.setSelected(false);
		
	}
	public void asignarPuerto(String puerto)
	{
		textFieldPuerto.setText(puerto);
	}
	public void asignarInstancia(String instancia)
	{
		textFieldInstacia.setText(instancia);
	}
}
