package Gui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import ConectorBD.ConexionBDOracle;
import Utils.StringMatcher;
import javax.swing.border.TitledBorder;

public class ActualizarSueldoEmpGui extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldNuevoSalario;
	private JTextField textFieldSalarioActual;
	private StartGui GUI;
	private JTextField textField_Empleado;
	private JComboBox comboBox_Id = new JComboBox();
	JLabel lblSalarioActual = new JLabel("Salario Actual:");
	private JTextField textFieldComisionActual;
	/**
	 * Create the frame.
	 */
	public ActualizarSueldoEmpGui() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane
						.showConfirmDialog(
								null,
								"�Estas seguro que deseas cerrar?",
								"Alerta", dialogButton);
				if (dialogResult == JOptionPane.YES_OPTION)
					{
						dispose();
						GUI.setEnabled(true);
						GUI.setVisible(true);
					} else if (dialogResult == JOptionPane.NO_OPTION)
					{
						System.out.println("Cancel the close window");
					}
			}
		});
		setResizable(false);
		setTitle("Actualizar Sueldo");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 557, 488);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("Archivo");
		mnArchivo.setFont(new Font("Segoe UI", Font.PLAIN, 17));
		menuBar.add(mnArchivo);
		
		JMenuItem mntmCerrar = new JMenuItem("Cerrar");
		mnArchivo.add(mntmCerrar);
		
		JMenuItem mntmActualizar = new JMenuItem("Actualizar");
		mnArchivo.add(mntmActualizar);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		lblSalarioActual.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblSalarioActual.setBounds(21, 95, 167, 28);
		contentPane.add(lblSalarioActual);
		
		textFieldSalarioActual = new JTextField();
		textFieldSalarioActual.setEditable(false);
		textFieldSalarioActual.setFont(new Font("Tahoma", Font.PLAIN, 16));
		textFieldSalarioActual.setColumns(10);
		textFieldSalarioActual.setBounds(200, 93, 312, 32);
		contentPane.add(textFieldSalarioActual);
		
		JButton btnActualizar = new JButton("Actualizar");
		btnActualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				StringMatcher s = new StringMatcher();
				boolean isId = s.isNumber(comboBox_Id.getSelectedItem().toString());
				boolean isSalary = s.isNumber(textFieldNuevoSalario.getText());
				if(isId && isSalary)
				{
					int id = Integer.parseInt(comboBox_Id.getSelectedItem().toString());
					int sueldoNuevo = Integer.parseInt(textFieldNuevoSalario.getText());
					
					boolean update =  GUI.getConexionOracle().actualizarSueldoPorIdEmp(id, sueldoNuevo);
					try {
						GUI.getConexionOracle().getConnection().commit();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if(update)
					{
						JOptionPane.showMessageDialog(ActualizarSueldoEmpGui.this, "Sueldo actualizado: "+sueldoNuevo ,"Informacion", JOptionPane.INFORMATION_MESSAGE);
						ActualizarSueldoEmpGui.this.dispose();
					}
					//else
						//JOptionPane.showMessageDialog(ActualizarSueldoEmpGui.this, "Hay un error al actualizar la informacion", "Error", JOptionPane.ERROR_MESSAGE);
				}
				else
					JOptionPane.showMessageDialog(ActualizarSueldoEmpGui.this, "Solo se aceptan datos numericos", "Error", JOptionPane.ERROR_MESSAGE);
			}
		});
		btnActualizar.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnActualizar.setBounds(12, 376, 108, 25);
		contentPane.add(btnActualizar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int dialogButton = JOptionPane.YES_NO_OPTION;
				int dialogResult = JOptionPane
						.showConfirmDialog(
								null,
								"�Estas seguro que deseas cerrar?",
								"Alerta", dialogButton);
				if (dialogResult == JOptionPane.YES_OPTION)
					{
					dispose();
					GUI.setEnabled(true);
					GUI.setVisible(true);
					} else if (dialogResult == JOptionPane.NO_OPTION)
					{
						System.out.println("Cancel the close window");
					}
			}
		});
		btnCancelar.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnCancelar.setBounds(415, 376, 97, 25);
		contentPane.add(btnCancelar);
		
		textField_Empleado = new JTextField();
		textField_Empleado.setEditable(false);
		textField_Empleado.setFont(new Font("Tahoma", Font.PLAIN, 16));
		textField_Empleado.setColumns(10);
		textField_Empleado.setBounds(200, 33, 312, 32);
		contentPane.add(textField_Empleado);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNombre.setBounds(21, 35, 167, 28);
		contentPane.add(lblNombre);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Datos actuales", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 11, 532, 250);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblIdentifiacionEmpleado = new JLabel("I.D. Empleado:");
		lblIdentifiacionEmpleado.setBounds(12, 194, 167, 28);
		panel.add(lblIdentifiacionEmpleado);
		lblIdentifiacionEmpleado.setFont(new Font("Tahoma", Font.PLAIN, 16));
		comboBox_Id.setBounds(191, 194, 312, 28);
		panel.add(comboBox_Id);
		comboBox_Id.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ConexionBDOracle oracle = GUI.getConexionOracle();
				int id=Integer.parseInt(comboBox_Id.getSelectedItem().toString());
				List<String> empleado = oracle.darListaEmpleadosId(id);
				List<String> comision = oracle.darListaComisionEmpleadosId(id);
				
				String nombre = empleado.get(0).substring(5);
				String com = comision.get(0);
				
				textField_Empleado.setText(nombre);
				textFieldComisionActual.setText(com);
				
				List<String> sueldo = oracle.darSalarioEmpleadoId(Integer.toString(id));
				textFieldSalarioActual.setText(sueldo.get(0));
			}
		});
		
		
		comboBox_Id.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		textFieldComisionActual = new JTextField();
		textFieldComisionActual.setFont(new Font("Tahoma", Font.PLAIN, 16));
		textFieldComisionActual.setEditable(false);
		textFieldComisionActual.setColumns(10);
		textFieldComisionActual.setBounds(191, 142, 312, 32);
		panel.add(textFieldComisionActual);
		
		JLabel lblComisionActual = new JLabel("Comision Actual:");
		lblComisionActual.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblComisionActual.setBounds(12, 144, 167, 28);
		panel.add(lblComisionActual);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "Actualizacion de salario", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(12, 274, 530, 89);
		contentPane.add(panel_1);
		
		JLabel lblNuevoSalario = new JLabel("Nuevo salario:");
		panel_1.add(lblNuevoSalario);
		lblNuevoSalario.setFont(new Font("Tahoma", Font.PLAIN, 16));
		
		textFieldNuevoSalario = new JTextField();
		panel_1.add(textFieldNuevoSalario);
		textFieldNuevoSalario.setFont(new Font("Tahoma", Font.PLAIN, 16));
		textFieldNuevoSalario.setColumns(10);
	}
	public void linkGui(StartGui startGui) {
		// TODO Auto-generated method stub
		this.GUI = startGui;
	}
	public void actualizarDatos() {
		ConexionBDOracle o = GUI.getConexionOracle();
		List<String> listaEmpleados = o.darListaEmpleadosId();
		
		for (String string : listaEmpleados) {
			comboBox_Id.addItem(string);
		}
		
	}
}
